# Project 6: Brevet time calculator service

Author: Alex Archer, aarcher@uoregon.edu

##This project is incomplete
I have submitted a minimal representation of the project for the 35/100 points.
I have spent far too long on this project there is some sort of workstation issue that I cannot troubleshoot. I will have to work in deschuttes exclusively for the last project as my personal machines do not work with the current level of support. 

I originally used WSL, that didn't work so I made a virtual box unbutu machine. I did not have enough disk space avaliable so the journal would not write and it would fail.
I bought an amazon EC2 instance with ubuntu and was still unable to get the program running. 
I tried adding in a shebang, removing the style definition from the .yml. I keep getting an OSError Exec format error for the debugger reluanch feature of werkzeug in flask. 
Based on github.com/pallets/werkzeug/issues/1482

I have been pulling my hair out just to get all the components to launch together. I can can project 5 to run, or the test website, but once I start combining services (though any number of .yml formats) there are unhelpful errors. Either OS Error Exec format error, or the mongodb fails during the build (no chnage from project 5) and they cannot link. I tried making a very basic flask representation that also failed. 

I have removed any test code for the most part so that the minimal provided dockerfile will run. Assumedly. 

TLDR; I give up!

## Grading Rubric

* If none of them work, you'll get 35 points assuming
    * README is updated with your name and email ID.
    * The credentials.ini is submitted with the correct URL of your repo.
    * Dockerfile is present. 
    * Docker-compose.yml works/builds without any errors.


