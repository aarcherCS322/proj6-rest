import flask
import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import acp_times
import arrow

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route('/')
@app.route("/index")
def index():
    #_items = db.tododb.find()
    #items = [item for item in _items]
    # return render_template('todo.html', items=items)
    return flask.render_template('calc.html')

@app.route('/display', methods=['POST']) #display
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    if items != []:
        return render_template('todo.html', items=items)
    else:
        return render_template('none.html')
@app.route('/reset', methods=['POST'])
def reset():
    db.tododb.remove({})
    return render_template('calc.html')

@app.route('/submit', methods=['POST'])
def submit():
    opentime = request.form.getlist('open')
    closetime = request.form.getlist('close')
    
    for each in range (10): #gosh I hope you dont have more than 10 controls
        if each != '':
            item_doc = {
                'opent': opentime[each],
                'closet': closetime[each]
            }
        db.tododb.insert_one(item_doc)

    return redirect(url_for('index'))

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    
    distance = request.args.get('distance',999, type=int)
    begin_date = request.args.get('begin_date',999, type=str)
    begin_time = request.args.get('begin_time',999, type=str)
    start_time_arrow = arrow.get('{}T{}'.format(begin_date, begin_time))
    start_time = start_time_arrow.isoformat() ##HERE??
    
    open_time = acp_times.open_time(km, distance, start_time)
    close_time = acp_times.close_time(km, distance, start_time)
    
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)
if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
